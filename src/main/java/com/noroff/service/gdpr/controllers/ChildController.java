package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.ChildRepository;
import com.noroff.service.gdpr.models.Child_parent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/child")
public class ChildController {

    ChildRepository childRepository = new ChildRepository();

    @GetMapping("/children/{username}")
    public ResponseEntity<ArrayList<Child_parent>> getChildOfParent(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Child_parent> childParent = childRepository.getChildOfParent(username);

        if(childParent == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(childParent, status);
    }

    @GetMapping("/parents/{username}")
    public ResponseEntity<ArrayList<Child_parent>> getParentsOfChild(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Child_parent> parents = childRepository.getParentsOfChild(username);

        if(parents == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(parents, status);
    }

}
