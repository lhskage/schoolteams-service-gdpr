package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.PersonRepository;
import com.noroff.service.gdpr.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/person")
public class PersonController {

    PersonRepository personRepository = new PersonRepository();

    @GetMapping("/username/{username}")
    public ResponseEntity<Person> getPersonByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Person person = personRepository.getPersonByUsername(username);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
