package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.GdprRepository;
import com.noroff.service.gdpr.models.GDPR;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/gdpr")
public class GdprController {

    GdprRepository gdprRepository = new GdprRepository();

    @GetMapping("{username}")
    public ResponseEntity<GDPR> getGdprByUsername(@PathVariable String username) {
        GDPR gdpr = gdprRepository.getGdprByUsername(username);

        HttpStatus status = HttpStatus.OK;

        return new ResponseEntity<>(gdpr, status);
    }
}
