package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.MessageRepository;
import com.noroff.service.gdpr.models.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/message")
public class MessageController {
    MessageRepository messageRepository = new MessageRepository();

    @GetMapping("/sent/{username}")
    public ResponseEntity<ArrayList<Message>> getSentMessages(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getSentMessages(username);

        if(messages == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/{username}")
    public ResponseEntity<ArrayList<Message>> getReceivedMessages(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getReceivedMessages(username);

        if(messages == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(messages, status);
    }

}
