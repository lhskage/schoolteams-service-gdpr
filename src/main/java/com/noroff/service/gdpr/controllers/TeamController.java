package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.TeamRepository;
import com.noroff.service.gdpr.models.Team;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/team")
public class TeamController {

    TeamRepository teamRepository = new TeamRepository();

    @GetMapping("/user/{username}")
    public ResponseEntity<ArrayList<Team>> getTeamByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamByUsername(username);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
