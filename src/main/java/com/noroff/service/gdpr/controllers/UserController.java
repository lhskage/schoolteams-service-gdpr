package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.UserRepository;
import com.noroff.service.gdpr.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserController {
    UserRepository userRepository = new UserRepository();

    @GetMapping("/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        User user = userRepository.getUser(username);

        if(user == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }
}