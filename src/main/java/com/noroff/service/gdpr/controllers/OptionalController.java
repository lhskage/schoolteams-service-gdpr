package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.OptionalRepository;
import com.noroff.service.gdpr.models.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/optional")
public class OptionalController {

    OptionalRepository optionalRepository = new OptionalRepository();

    @GetMapping("/user/{username}")
    public ResponseEntity<Optional> getSpecificOptionalInformation(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getSpecificOptionalInformation(username);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

}
