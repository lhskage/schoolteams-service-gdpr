package com.noroff.service.gdpr.controllers;

import com.noroff.service.gdpr.data_access.MatchRepository;
import com.noroff.service.gdpr.models.MatchDetailed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/matches")
public class MatchController {
    MatchRepository matchRepository = new MatchRepository();

    @GetMapping("/upcoming/user/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetailed = matchRepository.getUpcomingMatchesByUsername(username);

        if(matchDetailed == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetailed, status);
    }

    @GetMapping("/previous/user/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetailed = matchRepository.getPreviousMatchesByUsername(username);

        if(matchDetailed == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetailed, status);
    }
}
