package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.GDPR;

public class GdprRepository {

    PersonRepository personRepository = new PersonRepository();
    UserRepository userRepository = new UserRepository();
    OptionalRepository optionalRepository = new OptionalRepository();
    MessageRepository messageRepository = new MessageRepository();
    TeamRepository teamRepository = new TeamRepository();
    MatchRepository matchRepository = new MatchRepository();
    ChildRepository childRepository = new ChildRepository();

    public GDPR getGdprByUsername(String username) {
        GDPR gdpr = new GDPR(
                personRepository.getPersonByUsername(username),
                optionalRepository.getSpecificOptionalInformation(username),
                userRepository.getUser(username),
                messageRepository.getSentMessages(username),
                messageRepository.getReceivedMessages(username),
                teamRepository.getTeamByUsername(username),
                matchRepository.getPreviousMatchesByUsername(username),
                matchRepository.getUpcomingMatchesByUsername(username),
                childRepository.getChildOfParent(username),
                childRepository.getParentsOfChild(username)
        );
        return gdpr;
    }
}