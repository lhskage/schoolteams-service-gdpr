package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.Optional;

import java.sql.*;

public class OptionalRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Optional getSpecificOptionalInformation(String username){
        Optional optionalInformation = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM optional " +
                    "JOIN person " +
                    "ON optional.id = person.optional_id " +
                    "JOIN users " +
                    "ON person.id = users.person_id " +
                    "WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){optionalInformation = new Optional(
                    result.getInt("id"),
                    result.getObject("date_of_birth", java.time.LocalDateTime.class),
                    result.getString("mobile_number"),
                    result.getString("profile_picture"),
                    result.getString("medical_notes")
            );}

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optionalInformation;
    }
}

