package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.Team;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TeamRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Team> getTeamByUsername(String username) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT DISTINCT id, name, school_id, coach, sport_id FROM team " +
                    "JOIN player " +
                    "ON team.id = player.team_id " +
                    "WHERE (team.coach = ?) " +
                    "OR (player.user_username = ?);");
            prep.setString(1, username);
            prep.setString(2, username);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }
}
