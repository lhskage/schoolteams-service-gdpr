package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public User getUser(String username) {
        User user = null;
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){ user = new User(
                result.getString("username"),
                result.getInt("person_id"),
                result.getInt("role_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return user;
    }
}
