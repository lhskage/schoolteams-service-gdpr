package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.Person;

import java.sql.*;

public class PersonRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Person getPersonByUsername(String username) {
        Person person = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM person " +
                    "JOIN users " +
                    "ON person.id = users.person_id " +
                    "WHERE users.username = ?;");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){person = new Person(
                    result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname"),
                    result.getString("email"),
                    result.getString("gender"),
                    result.getInt("optional_id")
            );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return person;
    }
}
