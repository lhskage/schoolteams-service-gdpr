package com.noroff.service.gdpr.data_access;



import com.noroff.service.gdpr.models.Message;

import java.sql.*;
import java.util.ArrayList;

public class MessageRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Message> getSentMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE sender = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }

    public ArrayList<Message> getReceivedMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE receiver = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }
}
