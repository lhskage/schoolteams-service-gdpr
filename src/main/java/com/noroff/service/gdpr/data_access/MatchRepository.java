package com.noroff.service.gdpr.data_access;

import com.noroff.service.gdpr.models.MatchDetailed;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MatchRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    // Get matches by username

    public ArrayList<MatchDetailed> getUpcomingMatchesByUsername(String username) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "start_time, location.street, location.number, location.name FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "ON match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "ON match.team_2_id = away.team_id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ? OR home_team.coach = ? " +
                            "OR away_team.coach = ?) " +
                            "AND match.start_time >= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            prep.setString(3, username);
            prep.setString(4, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name")
                ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesByUsername(String username) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "ON match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "ON match.team_2_id = away.team_id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ? OR home_team.coach = ? " +
                            "OR away_team.coach = ?) " +
                            "AND match.start_time <= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            prep.setString(3, username);
            prep.setString(4, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

}
