package com.noroff.service.gdpr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;

@EnableEurekaClient
@Configuration
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class GdprApplication {
	public static void main(String[] args) {
		SpringApplication.run(GdprApplication.class, args);
	}
}