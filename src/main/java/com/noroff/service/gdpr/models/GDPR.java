package com.noroff.service.gdpr.models;

import java.util.List;

public class GDPR {
    private Person person;
    private Optional optional;
    private User user;
    private List<Message> sentMessages;
    private List<Message> receivedMessages;
    private List<Team> teams;
    private List<MatchDetailed> previousMatches;
    private List<MatchDetailed> upcomingMatches;
    private List<Child_parent> children;
    private List<Child_parent> parents;

    // Constructor
    public GDPR(Person person, Optional optional, User user, List<Message> sentMessages,
                List<Message> receivedMessages, List<Team> teams, List<MatchDetailed> previousMatches,
                List<MatchDetailed> upcomingMatches, List<Child_parent> children, List<Child_parent> parents) {
        this.person = person;
        this.optional = optional;
        this.user = user;
        this.sentMessages = sentMessages;
        this.receivedMessages = receivedMessages;
        this.teams = teams;
        this.previousMatches = previousMatches;
        this.upcomingMatches = upcomingMatches;
        this.children = children;
        this.parents = parents;
    }

    public GDPR() {
    }

    // Getters and setters
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Optional getOptional() {
        return optional;
    }

    public void setOptional(Optional optional) {
        this.optional = optional;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Message> getSentMessages() {
        return sentMessages;
    }

    public void setSentMessages(List<Message> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public List<Message> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(List<Message> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<MatchDetailed> getPreviousMatches() {
        return previousMatches;
    }

    public void setPreviousMatches(List<MatchDetailed> previousMatches) {
        this.previousMatches = previousMatches;
    }

    public List<MatchDetailed> getUpcomingMatches() {
        return upcomingMatches;
    }

    public void setUpcomingMatches(List<MatchDetailed> upcomingMatches) {
        this.upcomingMatches = upcomingMatches;
    }

    public List<Child_parent> getChildren() {
        return children;
    }

    public void setChildren(List<Child_parent> children) {
        this.children = children;
    }

    public List<Child_parent> getParents() {
        return parents;
    }

    public void setParents(List<Child_parent> parents) {
        this.parents = parents;
    }
}