package com.noroff.service.gdpr.models;

public class MatchResult {
    private int home_team_goals;
    private int away_team_goals;
    private String winner;
    private int match_id;

    // Constructors
    public MatchResult(int home_team_goals, int away_team_goals, String winner, int match_id) {
        this.home_team_goals = home_team_goals;
        this.away_team_goals = away_team_goals;
        this.winner = winner;
        this.match_id = match_id;
    }

    public MatchResult() {
    }

    // Getters and setters
    public int getHome_team_goals() {
        return home_team_goals;
    }

    public void setHome_team_goals(int home_team_goals) {
        this.home_team_goals = home_team_goals;
    }

    public int getAway_team_goals() {
        return away_team_goals;
    }

    public void setAway_team_goals(int away_team_goals) {
        this.away_team_goals = away_team_goals;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public int getMatch_id() {
        return match_id;
    }

    public void setMatch_id(int match_id) {
        this.match_id = match_id;
    }

}
