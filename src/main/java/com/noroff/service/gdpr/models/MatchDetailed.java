package com.noroff.service.gdpr.models;

import java.time.LocalDateTime;

public class MatchDetailed {
    private int match_id;
    private String home_team;
    private String away_team;
    private int home_team_goals;
    private int away_team_goals;
    private String winner;
    private LocalDateTime start_time;
    private String street_name;
    private String street_number;
    private String field_name;
    private String comment;

    // Constructors
    public MatchDetailed(String home_team, String away_team, int home_team_goals, int away_team_goals, String winner,
                         LocalDateTime start_time, String street_name, String street_number, String field_name, String comment) {
        this.home_team = home_team;
        this.away_team = away_team;
        this.home_team_goals = home_team_goals;
        this.away_team_goals = away_team_goals;
        this.winner = winner;
        this.start_time = start_time;
        this.street_name = street_name;
        this.street_number = street_number;
        this.field_name = field_name;
        this.comment = comment;
    }

    public MatchDetailed(int match_id, String home_team, String away_team, int home_team_goals, int away_team_goals,
                         String winner, LocalDateTime start_time, String street_name, String street_number, String field_name, String comment) {
        this.match_id = match_id;
        this.home_team = home_team;
        this.away_team = away_team;
        this.home_team_goals = home_team_goals;
        this.away_team_goals = away_team_goals;
        this.winner = winner;
        this.start_time = start_time;
        this.street_name = street_name;
        this.street_number = street_number;
        this.field_name = field_name;
        this.comment = comment;
    }

    public MatchDetailed(int match_id, String home_team, String away_team, int home_team_goals, int away_team_goals,
                         String winner, LocalDateTime start_time, String street_name, String street_number, String field_name) {
        this.match_id = match_id;
        this.home_team = home_team;
        this.away_team = away_team;
        this.home_team_goals = home_team_goals;
        this.away_team_goals = away_team_goals;
        this.winner = winner;
        this.start_time = start_time;
        this.street_name = street_name;
        this.street_number = street_number;
        this.field_name = field_name;

    }

    public MatchDetailed(int match_id, String home_team, String away_team, LocalDateTime start_time, String street_name, String street_number, String field_name) {
        this.match_id = match_id;
        this.home_team = home_team;
        this.away_team = away_team;
        this.start_time = start_time;
        this.street_name = street_name;
        this.street_number = street_number;
        this.field_name = field_name;
    }



    public MatchDetailed() {
    }



    // Getters and setters
    public int getMatch_id() {
        return match_id;
    }

    public void setMatch_id(int match_id) {
        this.match_id = match_id;
    }

    public String getHome_team() {
        return home_team;
    }

    public void setHome_team(String home_team) {
        this.home_team = home_team;
    }

    public String getAway_team() {
        return away_team;
    }

    public void setAway_team(String away_team) {
        this.away_team = away_team;
    }

    public int getHome_team_goals() {
        return home_team_goals;
    }

    public void setHome_team_goals(int home_team_goals) {
        this.home_team_goals = home_team_goals;
    }

    public int getAway_team_goals() {
        return away_team_goals;
    }

    public void setAway_team_goals(int away_team_goals) {
        this.away_team_goals = away_team_goals;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_number() {
        return street_number;
    }

    public void setStreet_number(String street_number) {
        this.street_number = street_number;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
