package com.noroff.service.gdpr.models;

import java.time.LocalDateTime;

public class Optional {
    private int id;
    private LocalDateTime date_of_birth;
    private String mobile_number;
    private String profile_picture;
    private String medical_notes;

    //Constructors
    public Optional(int id, LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes) {
        this.id = id;
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
    }

    public Optional(LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes) {
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
    }

    public Optional() {
    }

    //Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDateTime date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getMedical_notes() {
        return medical_notes;
    }

    public void setMedical_notes(String medical_notes) {
        this.medical_notes = medical_notes;
    }
}
